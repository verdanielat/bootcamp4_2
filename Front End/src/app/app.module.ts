import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';               
import { HttpModule } from '@angular/http';                 

import { APIService } from './services/api.service';                 

import { AppComponent } from './app.component';
import { CourseListComponent } from './courselist/courselist.component';



@NgModule({
  declarations: [
    AppComponent,
    CourseListComponent
  ],
  imports: [
    BrowserModule,HttpModule, FormsModule
  ],
  providers: [APIService],                     
  bootstrap: [AppComponent]
})
export class AppModule { }
