import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
@Component({
  selector: 'app-courselist',
  templateUrl: './courselist.component.html',
  styleUrls: ['./courselist.component.css']
})
export class CourseListComponent implements OnInit {

    courseList:object[];

  constructor(private service:APIService) { }


  ngOnInit() {
    this.courseList = this.service.courseList;
  }
  course_name:string="";
  class_room:string="";
  schedule:string="";


add(){

   let id = 1;
  if(this.courseList.length > 0){

    id = this.courseList[this.courseList.length-1]["id"]++;

  }


    this.courseList.push({

      'id': id, "course_name": this.course_name, "class_room":this.class_room , "schedule":this.schedule},

    );

    this.course_name = "";
    this.class_room = "";
    this.schedule = "";


  } 

}
