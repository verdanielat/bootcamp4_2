<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class MahasiswaController extends Controller
{
    
    function addStudent(Request $request)			
    {
    	
    	DB::beginTransaction();						

    	try
    	{
    		$this->validate($request, 										
    			['mahasiswa_name' => 'required' , 'email' => 'required|email']		// $request di validasi kalo namanya harus ada(require).
    			);

				if(isset($request->id)){

    		$std = Mahasiswa::find($request->id);
    		$std->mahasiswa_name =$request->input('mahasiswa_name');
    		$std->phonenumber = $request->input('phonenumber');
    		$std->email = $request->input('email');
    		$std->save();                                       

				}else{
			$Newstd = new Mahasiswa;
    		$Newstd->mahasiswa_name =$request->input('mahasiswa_name');
    		$Newstd->phonenumber = $request->input('phonenumber');
    		$Newstd->email = $request->input('email');
    		$Newstd->save();       
				}

    		DB::commit();
		return response()->json(["message"=>"Success"], 201);			
    	}
    	
    	catch(\Exception $e)
    	{
    		DB::rollback();
    		return response()->json(["message" => $e->getMessage()], 500);			// untuk verikasi gagal
    	}
    }

}
